<p align="center">
<img src="/branding/logo.png" alt="PPP">
</p>

**Plex Playlist Pusher (Podman) Re-write)**

*Looking for the original Bash version? Find it [here](https://github.com/XDGFX/PPP/tree/master)*

---

A simple Python 3 script used to automatically:

- load .m3u playlists from a local directory (maybe your Jellyfin library... **Can't be the same directory as your PPP installation!**)
- load music playlists from Plex
- compare the two, merging any new tracks or entire playlists
- push the updated playlists back to Plex using the [Plex Playlist API](https://forums.plex.tv/t/can-plexamp-read-and-use-m3u-playlists/234179/21)
- copy the updated playlists back to your local directory

*NOTE: This will only export non-Smart playlists. Any playlists created with smart filters will not be exported.*

This will keep Plex playlists and local playlists synchronised.
If you want to delete a playlist or song from a playlist, it must be removed from BOTH local and Plex playlists.

---

## Requirements

*Everything should work in both podman and docker, but I only test fully with podman!*

- [Podman](https://podman.io/) or [Docker](https://www.docker.com/)
- [podman-compose](https://github.com/containers/podman-compose) or [docker-compose](https://docs.docker.com/compose/)

---

## Configuration

1. Copy one of the `examples/*.json` to `variables.json`
2. Edit the settings in `variables.json` to match your specifics
3. Ensure that `docker-compose.yml` has volume mounts to the directories that match your Plex Server directories and where you want to save off your playlists

```
version: '3.5'

services:
  ppp:
    image: registry.gitlab.com/oneguynick/ppp:podman
    hostname: ppp
    container_name: ppp
    volumes:
      - ppp-data:/scratch
      - $PWD/examples/variables.json:/variables.json <--- ENSURE THIS IS COPIED!
      - /PLEXSERVER:/PLEXSERVER <--- SET THE DIRECTORY CORRECTLY
      - /PLAYLISTS:/PLAYLISTS <--- SET THE DIRECTORY CORRECTLY

volumes:
  ppp-data:
```

---

## Variables (Reference)
Running setup should help you find all these variables!

| VARIABLE | DESCRIPTION | EXAMPLE |
|---|---|---|
| `server_url` | the url of your Plex server, as seen by whatever you're running PPP on | `"http://192.168.1.100:32400"` |
| `check_ssl` | validate, or ignore SSL certificate ('"False"' for self signed https) | `"True"` |
| `plex_token` | find it [here](https://support.plex.tv/articles/204059436-finding-an-authentication-token-x-plex-token/) | `"A1B3c4bdHA3s8COTaE3l"` |
| `local_playlists` | path to the local playlists you want to use, relative to PPP | `"/mnt/Playlists"` |
| `working_directory` | path to PPP working directory, a directory accessible by both PPP and Plex | `"/mnt/PPP"` |
| `working_directory_plex` | path to PPP working directory as seen by Plex. Change it if Plex is running in a container and cannot see `working_directory` | `"/data/mnt/PPP"` |
| `section_id` | the library section which contains all your music (only one section is supported by the Plex API) | `"11"` |
| `local_prepend` | path to be ignored in local playlists | `"Z:\\Media\\Music\\"` |
| `plex_prepend` | path to be ignored in Plex playlists | `"/mnt/Media/Music"` |
| `local_convert` | only if local playlists are in a different directory format to your PPP machine | `"w2u"` |
| `plex_convert` | only if you Plex playlists are in a different directory format to your PPP machine | `False` |
| `OneWaySync` | sets if you will be just exporting playlists or syncing between the m3u and Plex. This is useful for editing the m3u with another program | 'True' |


    --- EXAMPLE LOCAL_PLAYLIST ---
    Z:\Media\Music\Andrew Huang\Love Is Real\Love Is Real.mp3
    Z:\Media\Music\Ben Howard\Noonday Dream\A Boat To An Island On The Wall.mp3
    Z:\Media\Music\Bibio\PHANTOM BRICKWORKS\PHANTOM BRICKWORKS.mp3

    --- EXAMPLE PLEX_PLAYLIST ---
    /mnt/media/Music/Andrew Huang/Love Is Real/Love Is Real.mp3
    /mnt/media/Music/Ben Howard/Noonday Dream/A Boat To An Island On The Wall.mp3
    /mnt/media/Music/Bibio/PHANTOM BRICKWORKS/PHANTOM BRICKWORKS.mp3


In the examples above, `local_prepend` is `"Z:\\Media\\Music\\"` and `plex_prepend` is `"/mnt/Media/Music"`

In this example, PPP is running on a machine which uses UNIX paths (/ not \\), and so `local_convert` is `"w2u"` - which means Windows paths are converted to UNIX paths.

If running PPP on Windows and your playlist paths are UNIX, use `"u2w"`, and if both paths are the same format use `false`.
