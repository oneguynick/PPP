help:
	@echo "Make Help"
	@echo "-----------------------"
	@echo ""
	@echo "Build podman images:"
	@echo "    make build"
	@echo ""
	@echo "Start podman suite:"
	@echo "    make start"
	@echo ""
	@echo "Stop podman suite:"
	@echo "    make stop"
	@echo ""
	@echo "Clear all dev code and test tools:"
	@echo "    make clean"
	@echo ""
	@echo "Run tests:"
	@echo "    make test"
	@echo ""
	@echo "Generate systemd file for scheduling"
	@echo "    make systemd"
	@echo ""
	@echo "See contents of Makefile for more targets."

all: build start

build:
	podman-compose build 

start:
	podman-compose up

stop:
	podman-compose down

systemd:
	podman-compose down

clean:
	rm -rf .cache
	rm -rf .pytest_cache
	find . -type f -name "*.pyc" -delete
	rm -rf $(find . -type d -name __pycache__)
	rm .coverage
	rm .coverage.*
	rm .pylint*

.PHONY: start stop clean build test
