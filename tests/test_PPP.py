import pytest
from PPP import convertPath, ConversionType

def test_unix_to_windows_conversion():
    unix_path = "/unix/path/file"
    expected_windows_path = "\\unix\\path\\file"
    assert convertPath(unix_path, ConversionType.UNIX_TO_WINDOWS) == expected_windows_path

def test_windows_to_unix_conversion():
    windows_path = "\\windows\\path\\file"
    expected_unix_path = "/windows/path/file"
    assert convertPath(windows_path, ConversionType.WINDOWS_TO_UNIX) == expected_unix_path

def test_mixed_path_conversion():
    mixed_path = "/mixed\\path/file"
    expected_unix_path = "/mixed/path/file"
    expected_windows_path = "\\mixed\\path\\file"
    assert convertPath(mixed_path, ConversionType.WINDOWS_TO_UNIX) == expected_unix_path
    assert convertPath(mixed_path, ConversionType.UNIX_TO_WINDOWS) == expected_windows_path

def test_inverted_conversion():
    path = "/unix/path/file"
    expected_windows_path = "\\unix\\path\\file"
    assert convertPath(path, ConversionType.WINDOWS_TO_UNIX, invert=True) == expected_windows_path

def test_none_input():
    assert convertPath(None, ConversionType.WINDOWS_TO_UNIX) is None
    assert convertPath(None, ConversionType.UNIX_TO_WINDOWS) is None
    assert convertPath(None, ConversionType.WINDOWS_TO_UNIX, invert=True) is None
